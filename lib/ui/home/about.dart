import 'package:eznior/internal/constants.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // GestureDetector(
            //   onTap: () {},
            //   child: Icon(
            //     Icons.sort,
            //     color: Colors.black,
            //   ),
            // ),
            // SizedBox(width: 16),
            Text(
              appName,
              style: TextStyle(
                  color: Colors.black, fontSize: 20, fontFamily: toolbarFont),
            )
          ],
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.shopping_basket,
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              SizedBox(
                height: 350,
              ),
              Text(
                'Aplikasi ini untuk memenuhi tugas Mobile Programing 2',
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 250,
              ),
              Container(
                padding: EdgeInsets.all(20),
                color: Colors.cyan[50],
                child: Text('CopyRight by Rafli Aryandi_18282016',
                    style: TextStyle(fontSize: 18)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
